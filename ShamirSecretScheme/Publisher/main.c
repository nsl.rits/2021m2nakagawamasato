#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <unistd.h>
#include <gmp.h>
#include <time.h>
#include <sys/time.h>

#include <signal.h>
#include <sys/stat.h>
#include <mosquitto.h>

#define DELIMITER "."
#define BLOCK_ALLOCATION_SIZE 1024

typedef struct MPZNode_t {
    struct MPZNode_t * next;
    mpz_t x;
    mpz_t y;
} MPZNode;

typedef struct {
    mpz_t * x_vals;
    mpz_t * y_vals;
    size_t num_shares;
} Shares;

typedef struct {
    Shares * chunks;
    size_t num_chunks;
} ShareChunks;

mpz_t modulus;
size_t modulus_max_bytes;
gmp_randstate_t rstate;

struct mosquitto *mosq;
typedef unsigned char byte;
const char
*msg, *msgA, *msgB, *msgC, *msgD, *msgE,
*msgF, *msgG, *msgH, *msgI, *msgJ,
*msgK, *msgL, *msgM, *msgN, *msgO,
*msgP, *msgQ, *msgR, *msgS, *msgT,
*msgU, *msgV, *msgW, *msgX, *msgY,
*msgZ, *msgAA, *msgAB, *msgAC, *msgAD,
*msgAE, *msgAF, *msgAG, *msgAH, *msgAI,
*msgAJ, *msgAK, *msgAL, *msgAM, *msgAN,
*msgAO, *msgAP, *msgAQ, *msgAR, *msgAS,
*msgAT, *msgAU, *msgAV, *msgAW, *msgAX,
*msgAY, *msgAZ, *msgBA, *msgBB, *msgBC,
*msgBD, *msgBE, *msgBF, *msgBG, *msgBH,
*msgBI, *msgBJ, *msgBK, *msgBL, *msgBM,
*msgBN, *msgBO, *msgBP, *msgBQ, *msgBR,
*msgBS, *msgBT, *msgBU, *msgBV, *msgBW,
*msgBX, *msgBY, *msgBZ, *msgCA, *msgCB,
*msgCC, *msgCD, *msgCE, *msgCF, *msgCG,
*msgCH, *msgCI, *msgCJ, *msgCK, *msgCL,
*msgCM, *msgCN, *msgCO, *msgCP, *msgCQ,
*msgCR, *msgCS, *msgCT, *msgCU, *msgCV;


unsigned char
addA[1024] = "1.", addB[1024] = "2.", addC[1024] = "3.", addD[1024] = "4.", addE[1024] = "5.",
addF[1024] = "6.", addG[1024] = "7.", addH[1024] = "8.", addI[1024] = "9.", addJ[1024] = "a.",
addK[1024] = "b.", addL[1024] = "c.", addM[1024] = "d.", addN[1024] = "e.", addO[1024] = "f.",
addP[1024] = "10.", addQ[1024] = "11.", addR[1024] = "12.", addS[1024] = "13.", addT[1024] = "14.",
addU[1024] = "15.", addV[1024] = "16.", addW[1024] = "17.", addX[1024] = "18.", addY[1024] = "19.",
addZ[1024] = "1a.", addAA[1024] = "1b.", addAB[1024] = "1c.", addAC[1024] = "1d.", addAD[1024] = "1e.",
addAE[1024] = "1f.", addAF[1024] = "20.", addAG[1024] = "21.", addAH[1024] = "22.", addAI[1024] = "23.",
addAJ[1024] = "24.", addAK[1024] = "25.", addAL[1024] = "26.", addAM[1024] = "27.", addAN[1024] = "28.",
addAO[1024] = "29.", addAP[1024] = "2a.", addAQ[1024] = "2b.", addAR[1024] = "2c.", addAS[1024] = "2d.",
addAT[1024] = "2e.", addAU[1024] = "2f.", addAV[1024] = "30.", addAW[1024] = "31.", addAX[1024] = "32.",
addAY[1024] = "33.", addAZ[1024] = "34.", addBA[1024] = "35.", addBB[1024] = "36.", addBC[1024] = "37.",
addBD[1024] = "38.", addBE[1024] = "39.", addBF[1024] = "3a.", addBG[1024] = "3b.", addBH[1024] = "3c.",
addBI[1024] = "3d.", addBJ[1024] = "3e.", addBK[1024] = "3f.", addBL[1024] = "40.", addBM[1024] = "41.",
addBN[1024] = "42.", addBO[1024] = "43.", addBP[1024] = "44.", addBQ[1024] = "45.", addBR[1024] = "46.",
addBS[1024] = "47.", addBT[1024] = "48.", addBU[1024] = "49.", addBV[1024] = "4a.", addBW[1024] = "4b.",
addBX[1024] = "4c.", addBY[1024] = "4d.", addBZ[1024] = "4e.", addCA[1024] = "4f.", addCB[1024] = "50.",
addCC[1024] = "51.", addCD[1024] = "52.", addCE[1024] = "53.", addCF[1024] = "54.", addCG[1024] = "55.",
addCH[1024] = "56.", addCI[1024] = "57.", addCJ[1024] = "58.", addCK[1024] = "59.", addCL[1024] = "5a.",
addCM[1024] = "5b.", addCN[1024] = "5c.", addCO[1024] = "5d.", addCP[1024] = "5e.", addCQ[1024] = "5f.",
addCR[1024] = "60.", addCS[1024] = "61.", addCT[1024] = "62.", addCU[1024] = "63.", addCV[1024] = "64.";

//////////////////////////////////////////////////////////////////////
//                            MQTT                                  //
//////////////////////////////////////////////////////////////////////

void endCatch(int sig) {
    mosquitto_destroy(mosq);
    mosquitto_lib_cleanup();
    printf("End Program\n");
    exit(0);
}

//////////////////////////////////////////////////////////////////////
//                    shamir secret scheme                          //
//////////////////////////////////////////////////////////////////////
/*モジュールの初期化*/
void initialize_with_modulus(mpz_t global_modulus, size_t max_bytes) {
    mpz_t max_secret_value;
    mpz_init(max_secret_value);
    mpz_ui_pow_ui(max_secret_value, 2, max_bytes*8);
    assert(mpz_cmp(global_modulus, max_secret_value) >= 0);
    mpz_clear(max_secret_value);

    // モジュールの設定
    mpz_set(modulus, global_modulus);
    modulus_max_bytes = max_bytes;

    // ランダムな初期化
    unsigned long int rseed = random();
    gmp_randinit_default(rstate);
    gmp_randseed_ui(rstate, rseed);
}

/*グローバル変数の初期化*/
void initialize() {
    mpz_t tmp;
    mpz_init(tmp);
    mpz_ui_pow_ui(tmp, 2, 521);
    mpz_sub_ui(tmp, tmp, 1);
    initialize_with_modulus(tmp, 64);
    mpz_clear(tmp);
}

/*メモリの解放*/
void cleanup() {
    mpz_clear(modulus);
    gmp_randclear(rstate);
}

void free_shares(Shares * shares) {
    for(size_t idx=0; idx<shares->num_shares; idx++) {
        mpz_clear(shares->x_vals[idx]);
        mpz_clear(shares->y_vals[idx]);
    }

    free(shares->x_vals);
    free(shares->y_vals);
    shares->x_vals = NULL;
    shares->y_vals = NULL;
    shares->num_shares = 0;
}

void free_chunks(ShareChunks * chunks) {
    if(! chunks || ! chunks->chunks)
        return;

    for(size_t idx=0; idx<chunks->num_chunks; idx++)
        free_shares(chunks->chunks+idx);

    free(chunks->chunks);
    chunks->chunks = NULL;
    chunks->num_chunks = 0;
}

/*encode*/
void encode_message(mpz_t value, const char * message) {
    printf("message = %s\n", message);
    mpz_import(value, strlen(message), 1, 1, 0, 0, message);
    printf("value = ");
    mpz_out_str(stdout, 10, value);
    printf("\n");
}

/*chunksの表示*/
void print_chunks(FILE * stream, const ShareChunks chunks) {
    // 空でないことの確認
    if(! chunks.num_chunks || ! chunks.chunks->num_shares)
        return;

    size_t num_chunks = chunks.num_chunks;                   // chunk数の取得
    size_t num_shares_per_chunk = chunks.chunks->num_shares; // chunkのシェア数を取得

    // 共有間の行方向ループ
    for(size_t i=0; i<num_shares_per_chunk; i++) {
        // 共有間の列方向ループ
        for(size_t j=0; j<num_chunks; j++) {
            Shares * shares = chunks.chunks+j;

            // 区切られたx, y値を出力
            mpz_out_str(stream, 16, shares->x_vals[i]);
            fprintf(stream, DELIMITER);
            mpz_out_str(stream, 16, shares->y_vals[i]);
            msg = mpz_get_str(0, 16, shares->y_vals[i]);

            switch(i) {
                case 0: strcat(addA, msg); break;
                case 1: strcat(addB, msg); break;
                case 2: strcat(addC, msg); break;
                case 3: strcat(addD, msg); break;
                case 4: strcat(addE, msg); break;
                case 5: strcat(addF, msg); break;
                case 6: strcat(addG, msg); break;
                case 7: strcat(addH, msg); break;
                case 8: strcat(addI, msg); break;
                case 9: strcat(addJ, msg); break;
                case 10: strcat(addK, msg); break;
                case 11: strcat(addL, msg); break;
                case 12: strcat(addM, msg); break;
                case 13: strcat(addN, msg); break;
                case 14: strcat(addO, msg); break;
                case 15: strcat(addP, msg); break;
                case 16: strcat(addQ, msg); break;
                case 17: strcat(addR, msg); break;
                case 18: strcat(addS, msg); break;
                case 19: strcat(addT, msg); break;
                case 20: strcat(addU, msg); break;
                case 21: strcat(addV, msg); break;
                case 22: strcat(addW, msg); break;
                case 23: strcat(addX, msg); break;
                case 24: strcat(addY, msg); break;
                case 25: strcat(addZ, msg); break;
                case 26: strcat(addAA, msg); break;
                case 27: strcat(addAB, msg); break;
                case 28: strcat(addAC, msg); break;
                case 29: strcat(addAD, msg); break;
                case 30: strcat(addAE, msg); break;
                case 31: strcat(addAF, msg); break;
                case 32: strcat(addAG, msg); break;
                case 33: strcat(addAH, msg); break;
                case 34: strcat(addAI, msg); break;
                case 35: strcat(addAJ, msg); break;
                case 36: strcat(addAK, msg); break;
                case 37: strcat(addAL, msg); break;
                case 38: strcat(addAM, msg); break;
                case 39: strcat(addAN, msg); break;
                case 40: strcat(addAO, msg); break;
                case 41: strcat(addAP, msg); break;
                case 42: strcat(addAQ, msg); break;
                case 43: strcat(addAR, msg); break;
                case 44: strcat(addAS, msg); break;
                case 45: strcat(addAT, msg); break;
                case 46: strcat(addAU, msg); break;
                case 47: strcat(addAV, msg); break;
                case 48: strcat(addAW, msg); break;
                case 49: strcat(addAX, msg); break;
                case 50: strcat(addAY, msg); break;
                case 51: strcat(addAZ, msg); break;
                case 52: strcat(addBA, msg); break;
                case 53: strcat(addBB, msg); break;
                case 54: strcat(addBC, msg); break;
                case 55: strcat(addBD, msg); break;
                case 56: strcat(addBE, msg); break;
                case 57: strcat(addBF, msg); break;
                case 58: strcat(addBG, msg); break;
                case 59: strcat(addBH, msg); break;
                case 60: strcat(addBI, msg); break;
                case 61: strcat(addBJ, msg); break;
                case 62: strcat(addBK, msg); break;
                case 63: strcat(addBL, msg); break;
                case 64: strcat(addBM, msg); break;
                case 65: strcat(addBN, msg); break;
                case 66: strcat(addBO, msg); break;
                case 67: strcat(addBP, msg); break;
                case 68: strcat(addBQ, msg); break;
                case 69: strcat(addBR, msg); break;
                case 70: strcat(addBS, msg); break;
                case 71: strcat(addBT, msg); break;
                case 72: strcat(addBU, msg); break;
                case 73: strcat(addBV, msg); break;
                case 74: strcat(addBW, msg); break;
                case 75: strcat(addBX, msg); break;
                case 76: strcat(addBY, msg); break;
                case 77: strcat(addBZ, msg); break;
                case 78: strcat(addCA, msg); break;
                case 79: strcat(addCB, msg); break;
                case 80: strcat(addCC, msg); break;
                case 81: strcat(addCD, msg); break;
                case 82: strcat(addCE, msg); break;
                case 83: strcat(addCF, msg); break;
                case 84: strcat(addCG, msg); break;
                case 85: strcat(addCH, msg); break;
                case 86: strcat(addCI, msg); break;
                case 87: strcat(addCJ, msg); break;
                case 88: strcat(addCK, msg); break;
                case 89: strcat(addCL, msg); break;
                case 90: strcat(addCM, msg); break;
                case 91: strcat(addCN, msg); break;
                case 92: strcat(addCO, msg); break;
                case 93: strcat(addCP, msg); break;
                case 94: strcat(addCQ, msg); break;
                case 95: strcat(addCR, msg); break;
                case 96: strcat(addCS, msg); break;
                case 97: strcat(addCT, msg); break;
                case 98: strcat(addCU, msg); break;
                case 99: strcat(addCV, msg); break;
                default: fprintf(stderr, "message over flow\n");
            }

            if(j < num_chunks-1)
                fprintf(stream, DELIMITER);
        }
        fprintf(stream, "\n");
    }
}

/*分割したメッセージシェアの作成*/
void split_into_shares(Shares * shares, const size_t k, const size_t n, const mpz_t secret) {
    // xとyにメモリを割り当てる
    shares->x_vals = malloc(n*sizeof(mpz_t));
    shares->y_vals = malloc(n*sizeof(mpz_t));
    shares->num_shares = n;

    printf("x_vals = "); mpz_out_str(stdout, 10, shares->x_vals); printf("\n");
    printf("y_vals = "); mpz_out_str(stdout, 10, shares->y_vals); printf("\n");

    // 多項式の作成
    mpz_t poly[k];
    for(size_t i=0; i<k; i++) {
        mpz_init(poly[i]);
        if(i == 0)
            mpz_set(poly[i], secret);
        else
            mpz_urandomm(poly[i], rstate, modulus);
    }

    // 一時的な変数の割り当て
    mpz_t x;
    mpz_t y;
    mpz_t buffer;
    mpz_init(x);
    mpz_init(y);
    mpz_init(buffer);

    // secret shares
    for(size_t j=0; j<n; j++) {
        mpz_set_ui(x, j+1);
        mpz_set_ui(y, 0);

        for(size_t i=0; i<k; i++) {
            mpz_powm_ui(buffer, x, i, modulus);
            mpz_mul(buffer, buffer, poly[i]);
            mpz_mod(buffer, buffer, modulus);
            mpz_add(y, y, buffer);
            mpz_mod(y, y, modulus);
        }

        // 値の更新
        mpz_init(shares->x_vals[j]);
        mpz_init(shares->y_vals[j]);
        mpz_set(shares->x_vals[j], x);
        mpz_set(shares->y_vals[j], y);
    }
    mpz_clear(x);
    mpz_clear(y);
    mpz_clear(buffer);
    for(size_t i=0; i<k; i++)
        mpz_clear(poly[i]);
}

/*分割*/
void split_into_chunks(ShareChunks * chunks, const size_t k, const size_t n, const char * message) {
    size_t message_length = strlen(message);

    // 0バイトの時
    if(! message_length) {
        return;
    }

    // chunksにメモリの割り当て
    chunks->num_chunks = (message_length-1)/modulus_max_bytes+1;
    chunks->chunks = malloc(chunks->num_chunks*sizeof(Shares));

    // secretブッファの初期化
    mpz_t secret;
    mpz_init(secret);

    size_t chunk_idx = 0;
    for(; chunk_idx<chunks->num_chunks; chunk_idx++) {
        size_t chunk_begin_offset = chunk_idx*modulus_max_bytes;
        size_t chunk_end_offset = chunk_begin_offset+modulus_max_bytes;

        if(chunk_end_offset > message_length)
            chunk_end_offset = message_length;

        size_t chunk_size = chunk_end_offset-chunk_begin_offset;

        char message_section[chunk_size+1];
        strncpy(message_section, message+chunk_begin_offset, chunk_size);
        message_section[chunk_size] = '\0';
        printf("message_section = %s\n", message_section);
        printf("secret = ");
        mpz_out_str(stdout, 10, secret);
        printf("\n");

        // Encode
        encode_message(secret, message_section);
        printf("secret = ");
        mpz_out_str(stdout, 10, secret);
        printf("\n");

        // シェアの作成
        split_into_shares(chunks->chunks+chunk_idx, k, n, secret);
    }

    mpz_clear(secret);
}

//////////////////////////////////////////////////////////////////////
//                              main                                //
//////////////////////////////////////////////////////////////////////
int main(int argc, char ** argv) {
    int split_mode = 0;
    int combine_mode = 0;
    opterr = 0;
    int c;
    struct timeval startTime, endTime;
    clock_t startClock, endClock;

    while((c = getopt(argc, argv, "sc")) != -1) {
        switch(c) {
            case 's' :
                split_mode = 1;
                break;
            case 'c' :
                combine_mode = 1;
                break;
            case '?' :
                if(isprint(optopt)) {
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                } else {
                    fprintf(stderr, "Unknown option character '\\x%x'.\n", optopt);
                }
                return EXIT_FAILURE;
            default :
                abort();
        }
    }

    // splitもcombineも指定されていない時
    if(split_mode == combine_mode) {
        if(split_mode)
            fprintf(stderr, "Please only provide one mode-setting flag.\n");
        else
            fprintf(stderr, "No flags were provided.\nPlease use -s for split mode and -c for combine mode.\n");
        return EXIT_FAILURE;
    }

    // 引数が足りない時
    if(optind+split_mode >= argc) {
        if(split_mode)
            fprintf(stderr, "k, n need to be provided.\n");
        else
            fprintf(stderr, "k, needs to be provided.\n");
        return EXIT_FAILURE;
    }

    char * end = NULL;
    long k = strtol(argv[optind], &end, 10);

    if(*end) {
        fprintf(stderr, "Please provide an integer calue for k.\n");
        return EXIT_FAILURE;
    }

    // 引数が1以下の時
    if(k < 1) {
        fprintf(stderr, "k must be strictly greater tham 0.\n");
        return EXIT_FAILURE;
    }

    if(split_mode) {
        long n = strtol(argv[optind+1], &end, 10);
        if(*end) {
            fprintf(stderr, "Please provide an integer value for n.\n");
            return EXIT_FAILURE;
        }
        if(k > n) {
            fprintf(stderr, "k must be less than or equal to n.\n");
            return EXIT_FAILURE;
        }

        //fprintf(stdout, "Generating shares using a (%ld, %ld) scheme with dynamic security level.\n", k, n);

        char * message = malloc(BLOCK_ALLOCATION_SIZE);     // メッセージ全てを保持するメモリブロック
        size_t message_allocation = BLOCK_ALLOCATION_SIZE;  // ブロックの現在のサイズ
        size_t message_idx = 0;                             // コピー先
        char * line = malloc(BLOCK_ALLOCATION_SIZE);        // getline()からの最後の読み取り行を保持するメモリブロック
        size_t line_allocation = BLOCK_ALLOCATION_SIZE;     // ラインブッファの現在のサイズ
        ssize_t line_length;                                // getline()から最後に読み込まれた文字数を示す
        char buf[BLOCK_ALLOCATION_SIZE];

        fprintf(stdout, "Enter the secret, at most 128 ASCII characters: ");
        if(! fgets(buf, sizeof(buf), stdin))
            fprintf(stderr, "I/O error while reading secret\n");
        buf[strcspn(buf, "\r\n")] = '\0';

        // グローバル変数の初期化
        initialize();

        // メッセージの分割
        ShareChunks chunks = {NULL, 0};
        //split_into_chunks(&chunks, k, n, message);

        gettimeofday(&startTime, NULL); // 開始時刻取得
        startClock = clock();          // 開始時刻のCPU時間取得

        split_into_chunks(&chunks, k, n, buf);

        print_chunks(stdout, chunks);

        free(line);
        free(message);
        free_chunks(&chunks);

        cleanup();

        const char *ip = "192.168.3.7"; //自宅
        //const char *ip = "192.168.1.71"; //研究室
        //const char *ip = "192.168.1.90";
        //const char *ip = "192.168.1.91"; //raspberry pi
        const int port = 1883;
        //const int port = 8883;
        const char *username = "topsecret";
        const char *password = "password";
        const char *id = "Macbookair";
        const char *topic = "both";
        //const char msg[32] = "Hello";
        const int sleepTime = 2;

        FILE *fp, *fp2;

        signal(SIGINT, endCatch);
        printf("Prepareting mosquitto...\n");
        mosquitto_lib_init();
        mosq = mosquitto_new(id, 0, NULL);

        if(! mosq) {
            perror("Failed to create mosquitto.\n");
            mosquitto_lib_cleanup();
            return EXIT_FAILURE;
        }


        mosquitto_username_pw_set(mosq, username, password);
        //mosquitto_tls_set(mosq, "/usr/local/opt/mosquitto/etc/mosquitto/ca.crt", NULL, NULL, NULL, NULL);
        mosquitto_connect(mosq, ip, port, 60);
        printf("Server connected!\n");

        mosquitto_publish(mosq, NULL, topic, strlen(addA), addA, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addB), addB, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addC), addC, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addD), addD, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addE), addE, 0, 0);
/*
        mosquitto_publish(mosq, NULL, topic, strlen(addF), addF, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addG), addG, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addH), addH, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addI), addI, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addJ), addJ, 0, 0);

        mosquitto_publish(mosq, NULL, topic, strlen(addK), addK, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addL), addL, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addM), addM, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addN), addN, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addO), addO, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addP), addP, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addQ), addQ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addR), addR, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addS), addS, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addT), addT, 0, 0);

        //sleep(1);

        mosquitto_publish(mosq, NULL, topic, strlen(addU), addU, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addV), addV, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addW), addW, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addX), addX, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addY), addY, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addZ), addZ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAA), addAA, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAB), addAB, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAC), addAC, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAD), addAD, 0, 0);

        mosquitto_publish(mosq, NULL, topic, strlen(addAE), addAE, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAF), addAF, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAG), addAG, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAH), addAH, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAI), addAI, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAJ), addAJ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAK), addAK, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAL), addAL, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAM), addAM, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAN), addAN, 0, 0);

        //sleep(1);

        mosquitto_publish(mosq, NULL, topic, strlen(addAO), addAO, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAP), addAP, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAQ), addAQ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAR), addAR, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAS), addAS, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAT), addAT, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAU), addAU, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAV), addAV, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAW), addAW, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAX), addAX, 0, 0);

        mosquitto_publish(mosq, NULL, topic, strlen(addAY), addAY, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addAZ), addAZ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBA), addBA, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBB), addBB, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBC), addBC, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBD), addBD, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBE), addBE, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBF), addBF, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBG), addBG, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBH), addBH, 0, 0);

        //sleep(1);

        mosquitto_publish(mosq, NULL, topic, strlen(addBI), addBI, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBJ), addBJ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBK), addBK, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBL), addBL, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBM), addBM, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBN), addBN, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBO), addBO, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBP), addBP, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBQ), addBQ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBR), addBR, 0, 0);

        mosquitto_publish(mosq, NULL, topic, strlen(addBS), addBS, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBT), addBT, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBU), addBU, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBV), addBV, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBW), addBW, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBX), addBX, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBY), addBY, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addBZ), addBZ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCA), addCA, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCB), addCB, 0, 0);

        //sleep(10);

        mosquitto_publish(mosq, NULL, topic, strlen(addCC), addCC, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCD), addCD, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCE), addCE, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCF), addCF, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCG), addCG, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCH), addCH, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCI), addCI, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCJ), addCJ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCK), addCK, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCL), addCL, 0, 0);

        mosquitto_publish(mosq, NULL, topic, strlen(addCM), addCM, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCN), addCN, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCO), addCO, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCP), addCP, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCQ), addCQ, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCR), addCR, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCS), addCS, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCT), addCT, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCU), addCU, 0, 0);
        mosquitto_publish(mosq, NULL, topic, strlen(addCV), addCV, 0, 0);
        */


        /*
        mosquitto_publish(mosq, NULL, topic, strlen(addA), addA, 0, 0);
        printf("Publish \"%s\" about \"%s\" from %s to %s : %d\n", addA, topic, id, ip, port);
        mosquitto_publish(mosq, NULL, topic, strlen(addB), addB, 0, 0);
        printf("Publish \"%s\" about \"%s\" from %s to %s : %d\n", addB, topic, id, ip, port);

        mosquitto_destroy(mosq);
        mosquitto_lib_cleanup();
        printf("End Program\n");
        printf("\n");
        sleep(1);

        ip = "192.168.1.90";
        printf("Prepareting mosquitto...\n");
        mosquitto_lib_init();
        mosq = mosquitto_new(id, 0, NULL);

        if(! mosq) {
            perror("Failed to create mosquitto.\n");
            mosquitto_lib_cleanup();
            return EXIT_FAILURE;
        }

        mosquitto_username_pw_set(mosq, username, password);
        //mosquitto_tls_set(mosq, "/usr/local/opt/mosquitto/etc/mosquitto/ca.crt", NULL, NULL, NULL, NULL);
        mosquitto_connect(mosq, ip, port, 60);

        printf("Server connected!\n");

        mosquitto_publish(mosq, NULL, topic, strlen(addC), addC, 0, 0);
        printf("Publish \"%s\" about \"%s\" from %s to %s : %d\n", addC, topic, id, ip, port);
        mosquitto_publish(mosq, NULL, topic, strlen(addD), addD, 0, 0);
        printf("Publish \"%s\" about \"%s\" from %s to %s : %d\n", addD, topic, id, ip, port);

        mosquitto_destroy(mosq);
        mosquitto_lib_cleanup();
        printf("End Program\n");
        printf("\n");
        sleep(1);

        ip = "192.168.1.91";
        printf("Prepareting mosquitto...\n");
        mosquitto_lib_init();
        mosq = mosquitto_new(id, 0, NULL);

        if(! mosq) {
            perror("Failed to create mosquitto.\n");
            mosquitto_lib_cleanup();
            return EXIT_FAILURE;
        }

        mosquitto_username_pw_set(mosq, username, password);
        //mosquitto_tls_set(mosq, "/usr/local/opt/mosquitto/etc/mosquitto/ca.crt", NULL, NULL, NULL, NULL);
        mosquitto_connect(mosq, ip, port, 60);

        mosquitto_publish(mosq, NULL, topic, strlen(addE), addE, 0, 0);
        printf("Publish \"%s\" about \"%s\" from %s to %s : %d\n", addE, topic, id, ip, port);

        mosquitto_destroy(mosq);
        mosquitto_lib_cleanup();
        printf("End Program\n");
        printf("\n");
        sleep(1);
        */

        gettimeofday(&endTime, NULL); // 開始時刻取得
        endClock = clock();           // 開始時刻のCPU時間

        time_t diffsec = difftime(endTime.tv_sec, startTime.tv_sec); // 差分処理
        suseconds_t diffsub = endTime.tv_usec-startTime.tv_usec;     // マイクロ秒部分の差分処理

        double cpusec = (endClock-startClock)/(double)CLOCKS_PER_SEC;
        fprintf(stdout, "CPU時間 : %f\n", cpusec);

        fp = fopen("cpuData.txt", "a");
        fprintf(fp, "%.4f\n", (double)cpusec);
        fclose(fp);

        fp2 = fopen("data.txt", "a");
        fprintf(fp2, "%ld.%.3ld\n", diffsec, diffsub);
        fclose(fp2);

    } else {
        fprintf(stderr, "set up mode only split\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
