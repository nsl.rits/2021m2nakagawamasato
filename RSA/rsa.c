#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <gmp.h>

#include <signal.h>
#include <sys/stat.h>
#include <mosquitto.h>

#define KEYBIT_LEN 1024
#define E_LEN 1024
#define MIN_P 49
#define MIN_Q 49
#define DIF_BET 4 //diffrent between P and Q
#define RABIN 25

#define rsa_encrypt(c, m, e, n) mpz_powm((c), (m), (e), (n))
#define rsa_decrypt(m, c, d, n) mpz_powm((m), (c), (d), (n))

char *inFile, *outFile;

struct mosquitto *mosq;
typedef unsigned char byte;

FILE *fp_enc;


static void printHexEnc(const char *title, const unsigned char *s, int len) {
    int n;

    fp_enc = fopen("enc.txt", "w");

    printf("%s: ", title);
    for(n=0; n<len; ++n) {
        if((n%16) == 0) {
            printf("\n%04x", n);
        }
        fprintf(fp_enc, "%02x", s[n]);
        printf(" %02x", s[n]);
    }
    printf("\n");
    fclose(fp_enc);
}

static void printHex(const char *title, const unsigned char *s, int len) {
    int n;

    printf("%s: ", title);
    for(n=0; n<len; ++n) {
        if((n%16) == 0) {
            printf("\n%04x", n);
        }
        printf(" %02x", s[n]);
    }
    printf("\n");
}
/*
int write_file(char *fname, unsigned char *data, int len) {
    FILE *fp;

    if(data == NULL || fname == NULL)
        return 1;
    if((fp = fopen(fname, "wb")) == NULL)
        return 1;
    if(fwrite(data, len, 1, fp) < 1) {
        fclose(fp);
        return 1;
    }

    printHex("DATA", data, len);

    fclose(fp);

    return 0;
}
*/

unsigned char* read_file(int *len, char *fname) {
    FILE *fp;
    struct stat sbuf;
    unsigned char *data;

    if(len == NULL || fname == NULL)
        return NULL;
    if(stat(fname, &sbuf) == -1)
        return NULL;

    *len = (int)sbuf.st_size;
    data = (unsigned char*)malloc(*len);

    if(!data)
        return NULL;
    if((fp = fopen(fname, "rb")) == NULL)
        return NULL;
    if(fread(data, *len, 1, fp) < 1) {
        fclose(fp);
        return NULL;
    }

    printHex("DATA", data, *len);

    fclose(fp);

    return data;
}


void mpz_random_max(mpz_ptr r, const mpz_ptr limit) {
    gmp_randstate_t state;
    time_t timer;

    gmp_randinit_default(state);
    time(&timer);

    gmp_randseed_ui(state, (unsigned long)timer);
    mpz_urandomm(r, state, limit);

    gmp_randclear(state);
}

void mpz_random_bit(mpz_ptr r, const int bits) {
    mpz_t limit;

    mpz_init(limit);
    mpz_setbit(limit, bits);
    mpz_random_max(r, limit);

    mpz_clear(limit);
}

void mpz_random_prime(mpz_ptr p, int bits) {
    int dir;
    mpz_t ran;
    gmp_randstate_t state;
    time_t timer;

    if(bits <= 1)
        return;

    gmp_randinit_default(state);
    time(&timer);
    gmp_randseed_ui(state, (unsigned long)timer);

    mpz_init(ran);
    mpz_urandomb(ran, state, 1);
    gmp_randclear(state);

    dir = (int)mpz_scan0(ran, 0);

    mpz_random_bit(p, bits);
    mpz_setbit(p, bits-1);
    mpz_setbit(p, 0);

    while(!mpz_probab_prime_p(p, RABIN)) {
        if(dir) {
            mpz_add_ui(p, p, 2);
            if(mpz_sizeinbase(p, 2) != (size_t)bits) {
                mpz_set_ui(p, 0);
                mpz_setbit(p, bits-1);
                mpz_setbit(p, 0);
            }
        } else {
            mpz_sub_ui(p, p, 2);
            if(mpz_sizeinbase(p, 2) != (size_t)bits) {
                mpz_set_ui(p, 0);
                mpz_setbit(p, bits);
                mpz_sub_ui(p, p, 1);
            }
        }
    }
}

void mpz_random_second_prime(mpz_ptr p, mpz_ptr pmax, mpz_ptr pmin) {
    int dir, isprime;
    int mxf = 0, mnf = 0;
    mpz_t c, ran;
    gmp_randstate_t state;
    time_t timer;

    if(mpz_cmp(pmax, pmin) <= 0)
        return;
    if(mpz_cmp_ui(pmax, 2) <= 0)
        return;
    if(mpz_cmp(pmax, pmin) == 0 && mpz_scan0(pmax, 0) == 0)
        return;

    isprime = 0;

    if(mpz_scan0(pmax, 0) == 0) {
        mxf = 1;
        mpz_add_ui(pmax, pmax, 1);
    }

    if(mpz_scan0(pmin, 0) == 0) {
        if(mpz_cmp_ui(pmin, 0) == 0) {
            mnf = 2;
            mpz_set_ui(pmin, 2);
        } else {
            mnf = 1;
            mpz_add_ui(pmin, pmin, 1);
        }
    }

    mpz_init(c);
    mpz_sub(c, pmax, pmin);

    do {
        mpz_random_max(p, c);
        mpz_add(p, p, pmin);
        mpz_setbit(p, 0);
    } while(mpz_cmp(pmax, p) <= 0);

    mpz_tdiv_q_2exp(c, c, 1);

    mpz_init(ran);

    gmp_randinit_default(state);
    time(&timer);
    gmp_randseed_ui(state, (unsigned long)timer);

    mpz_urandomb(ran, state, 1);
    gmp_randclear(state);

    dir = (int)mpz_scan0(ran, 0);

    do {
        isprime = mpz_probab_prime_p(p, RABIN);
        if(isprime)
            break;

        mpz_sub_ui(c, c, 1);

        if(dir) {
            mpz_add_ui(p, p, 2);
            if(mpz_cmp(p, pmax) >= 0) {
                mpz_set(p, pmin);
            }
        } else {
            mpz_sub_ui(p, p, 2);
            if((mpz_cmp(p, pmin) < 0) || mpz_cmp_ui(p, 1) == 0) {
                mpz_set(p, pmax);
            }
        }
    } while(mpz_cmp_ui(c, 0) != 0);

    if(isprime) {
        if(mpz_cmp(p, pmin) < 0 || mpz_cmp(pmax, p) < 0) {
            fprintf(stderr, "Fatal Internal Error\n");
            fprintf(stderr, "P = ");
            mpz_out_str(stderr, 16, p);
            puts("");
            fprintf(stderr, "pmax = ");
            mpz_out_str(stderr, 16, pmax);
            puts("");
            fprintf(stderr, "pmin = ");
            mpz_out_str(stderr, 16, pmin);
            puts("");
            exit(1);
        }
    }

    if(mxf)
        mpz_sub_ui(pmax, pmax, 1);
    if(mnf)
        mpz_sub_ui(pmin, pmin, mnf);

    mpz_clear(c);
}

int generate_prime(mpz_ptr n, mpz_ptr p, mpz_ptr q, int pb, int qb) {
    int nb;
    mpz_t tp, tq, maxq, minq, tmp, ptr;

    if(pb < MIN_P)
        return 1;
    if(qb < MIN_Q)
        return 1;

    mpz_init(tp);
    mpz_init(tq);
    mpz_init(maxq);
    mpz_init(minq);
    mpz_init(tmp);
    mpz_init(ptr);

    nb = pb + qb;

    mpz_random_prime(tp, pb);

    mpz_set_ui(tmp, 0);
    mpz_setbit(tmp, nb);
    mpz_sub_ui(tmp, tmp, 1);
    mpz_tdiv_q(maxq, tmp, tp);
    mpz_set_ui(tmp, 0);
    mpz_setbit(tmp, qb);

    if(mpz_cmp(maxq, tmp) > 0) {
        mpz_set(maxq, tmp);
        mpz_sub_ui(maxq, maxq, 1);
    }

    mpz_set_ui(tmp, 0);
    mpz_setbit(tmp, nb-1);
    mpz_tdiv_q(minq, tmp, tp);
    mpz_set_ui(tmp, 0);
    mpz_setbit(tmp, qb-1);

    if(mpz_cmp(minq, tmp) < 0) {
        mpz_set(minq, tmp);
    }

    do {
        mpz_random_second_prime(tq, maxq, minq);
        mpz_sub(tmp, tp, tq);
    } while(pb-mpz_sizeinbase(tmp, 2) > DIF_BET);

    if(mpz_sgn(tmp) == -1 && pb == qb) {
        mpz_set(ptr, tp);
        mpz_set(tp, tq);
        mpz_set(tq, ptr);
    }

    mpz_mul(n, tp, tq);
    mpz_set(p, tp);
    mpz_set(q, tq);

    mpz_clear(tp);
    mpz_clear(tq);
    mpz_clear(maxq);
    mpz_clear(minq);
    mpz_clear(tmp);
    mpz_clear(ptr);

    return 0;
}

int generate_key(mpz_ptr d, mpz_ptr e, int eb, mpz_ptr p, mpz_ptr q) {
    mpz_t lm, p1, q1;
    int rv;

    if(eb < 2)
        return 1;

    mpz_init(lm);
    mpz_init_set(p1, p);
    mpz_sub_ui(p1, p1, 1);
    mpz_init_set(q1, q);
    mpz_sub_ui(q1, q1, 1);

    mpz_lcm(lm, p1, q1);

    do {
        mpz_random_prime(e, eb);
        rv = mpz_invert(d, e, lm);
    } while(!rv);

    if(mpz_sgn(d) == -1) {
        mpz_add(d, d, lm);
    }

    mpz_clear(lm);
    mpz_clear(p1);
    mpz_clear(q1);

    return 0;
}

void mpz_mptoc(unsigned char *ch, unsigned int *n, mpz_ptr a) {
    unsigned int i, j, size;
    char *str;
    unsigned char tmp1, tmp2;

    size = (unsigned int)mpz_sizeinbase(a, 16);
    str = (char *)malloc(size+1);
    mpz_get_str(str, 16, a);

    i = 0;
    j = 0;
    tmp1 = 0;

    if(size % 2) {
        if(str[i] >= '0' && str[i] <= '9') {
            tmp1 = str[i++]-'0';
        } else if(str[i] >= 'A' && str[i] <= 'F') {
            tmp1 = str[i++]-'A'+0xA;
        } else if(str[i] >= 'a' && str[i] <= 'f') {
            tmp1 = str[i++]-'a'+0xA;
        }
        ch[j++] = tmp1 & 0xf;
    }

    for(; i<size; i+=2) {
        tmp1 = 0;
        tmp2 = 0;
        if(str[i] >= '0' && str[i] <= '9') {
            tmp1 = str[i]-'0';
        } else if(str[i] >= 'A' && str[i] <= 'F') {
            tmp1 = str[i]-'A'+0xA;
        } else if(str[i] >= 'a' && str[i] <= 'f') {
            tmp1 = str[i]-'a'+0xA;
        }
        if(i+1 < size) {
            tmp1 <<= 4;
            if(str[i+1] >= '0' && str[i+1] <= '9') {
                tmp2 = str[i+1]-'0';
            } else if(str[i+1] >= 'A' && str[i+1] <= 'F') {
                tmp2 = str[i+1]-'A'+0xA;
            } else if(str[i+1] >= 'a' && str[i+1] <= 'f') {
                tmp2 = str[i+1]-'a'+0xA;
            }
        }
        ch[j++] = tmp1 | (tmp2 & 0xf);
    }
    *n = j;
    free(str);
}

void mpz_ctomp(mpz_ptr a, unsigned char *ch, unsigned int n) {
    unsigned int i;
    char *str;
    char num_to_text[] = "0123456789abcdef";

    str = (char *)malloc(2*n+1);

    for(i=0; i<n; i++) {
        str[2*i] = num_to_text[ch[i] >> 4];
        str[2*i+1] = num_to_text[ch[i] & 0xf];
    }

    str[2*n] = 0;
    mpz_set_str(a, str, 16);
}

int main(int argc, char **argv) {
    unsigned int i;
    unsigned char *intext, *outtext;
    unsigned int inLen = 0, ivLen = 16, encLen, decLen;
    unsigned char enc[1024], dec[1024];

    mpz_t p, q, d, n, e;
    mpz_t c, m, l;
    int pb, qb, eb;

    struct timeval startTime, endTime;
    clock_t startClock, endClock;
    FILE *fp;

    inFile = argv[1];

    intext = read_file(&inLen, inFile);
    //outtext = malloc(inLen + ivLen);

    gettimeofday(&startTime, NULL);
    startClock = clock();

    printf("<RSA Key Generation>\n");

    pb = qb = KEYBIT_LEN/2;
    eb = E_LEN;

    mpz_init(p);
    mpz_init(q);
    mpz_init(d);
    mpz_init(n);
    mpz_init(e);

    generate_prime(n, p, q, pb, qb);
    generate_key(d, e, eb, p, q);

    printf("p = "); mpz_out_str(stdout, 16, p); printf("\n");
    printf("q = "); mpz_out_str(stdout, 16, q); printf("\n");
    printf("n = "); mpz_out_str(stdout, 16, n); printf("\n");
    printf("e = "); mpz_out_str(stdout, 16, e); printf("\n");
    printf("d = "); mpz_out_str(stdout, 16, d); printf("\n");
    printf("\n");

    mpz_init(c);
    mpz_init(m);

    printf("<RSA Encrypt/Decrypt>\n");
    printHex("PLAIN", intext, inLen);

    mpz_ctomp(m, intext, inLen);
    rsa_encrypt(c, m, e, n);
    mpz_mptoc(enc, &encLen, c);

    printHexEnc("ENCRYPT", enc, encLen);

    mpz_init(l);
    rsa_decrypt(l, c, d, n);
    mpz_mptoc(dec, &decLen, l);

    printHex("DECRYPT", dec, decLen);

    printf("inLen = %d\n", inLen);
    printf("decLen = %d\n", decLen);

    gettimeofday(&endTime, NULL);
    endClock = clock();

    time_t diffsec = difftime(endTime.tv_sec, startTime.tv_sec); // 差分処理
    suseconds_t diffsub = endTime.tv_usec-startTime.tv_usec;     // マイクロ秒部分の差分処理
    fprintf(stdout, "計測時間 : %ld.%.4ld\n", diffsec, diffsub);

    double cpusec = (double)(endClock-startClock)/(double)CLOCKS_PER_SEC;
    fprintf(stdout, "CPU時間 : %.4f\n", cpusec);


    fp = fopen("data.txt", "a");
    fprintf(fp, "%ld.%.4ld\n", diffsec, diffsub);
    fclose(fp);

    mosquitto();


    mpz_clear(p);
    mpz_clear(q);
    mpz_clear(d);
    mpz_clear(e);
    mpz_clear(n);
    mpz_clear(c);
    mpz_clear(m);
    mpz_clear(l);
}
